//
//  ViewController.m
// Trivia for ios

#import "LevelViewController.h"
#import "LearnViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "Function.h"
#import "staticstrings.h"
#import "AppDelegate.h"
@interface LevelViewController ()

@end

@implementation LevelViewController
{
    NSMutableArray *qustns;
    NSMutableArray *category_id;
    NSMutableArray *categoryArray;
    int count;
    AppDelegate *session;
}
@synthesize home;

- (void)viewDidLoad {
    session = [[UIApplication sharedApplication]delegate];
    [super viewDidLoad];
    qustns =  [[NSMutableArray alloc]init];
    categoryArray =  [[NSMutableArray alloc]init];
    category_id = [[NSMutableArray alloc]init];
    NSString *isPlayLocal = [staticstrings getIsLocalPlay];
    
    if ([isPlayLocal isEqualToString:@"true"]) {
        categoryArray = [ Function getLevelInfo];
        for (int i=0; i<categoryArray.count; i++) {
            NSString *categoryName = [[categoryArray objectAtIndex:i] objectForKey:@"Level_name"];        //Get Level Name from Server database
            NSString *categoryid = [[categoryArray objectAtIndex:i] objectForKey:@"Level_id"];            //Get Level No. from Server database
            [qustns addObject: categoryName];
            [category_id addObject:categoryid];
        }
    }else{
       categoryArray = [self GetLevelList];
       for (int i=0; i<categoryArray.count; i++) {
          NSString *categoryName = [[categoryArray objectAtIndex:i] objectForKey:@"levelname"];         //Get Level Name from Local database
          NSString *categoryid = [[categoryArray objectAtIndex:i] objectForKey:@"lelveno"];             //Get Level No. from Local database
          [qustns addObject: categoryName];
          [category_id addObject:categoryid];
        }
    }
 
}

//Get Level_Name and Level_no. from Server
-(NSMutableArray*)GetLevelList
{
    NSString *questionurl = [NSString stringWithFormat:@"%@",[staticstrings getLevel] ];
    NSMutableString *URL1 = [[NSMutableString alloc]initWithString:questionurl];
    NSURL *url1 = [NSURL URLWithString:URL1];
    NSError *error1 = nil;
    NSStringEncoding encoding1;
    NSString *jsonreturn1 = [[NSString alloc] initWithContentsOfURL:url1 usedEncoding:&encoding1 error:&error1];
    
    if (!jsonreturn1)
    {
        UIAlertView *alertView1 = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                             message:@"There is no server response please try again later."
                                                            delegate:self
                                                   cancelButtonTitle:nil
                                                   otherButtonTitles:@"OK" ,nil];
        [alertView1 show];
        [alertView1 release];
        return nil;
    }
    else
    {
        NSMutableArray *dict1 = [jsonreturn1 JSONValue];
        return dict1;
    }
}


-(void)savedataLocally
{
    for (int i = 0; i<categoryArray.count; i++)
    {
        [DBOperation InsertLevel:[[categoryArray objectAtIndex:i] valueForKey:@"levelname" ] levelId:[[categoryArray objectAtIndex:i] valueForKey:@"lelveno" ]];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [qustns count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    tableView.backgroundColor=[UIColor clearColor];
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.text = [qustns objectAtIndex:indexPath.row];
    cell.backgroundColor = [UIColor clearColor];
    [[cell layer] setBorderColor:[[UIColor colorWithRed:255/255.0 green:179/255.0 blue:0/255.0 alpha:1] CGColor]];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}

//Go to LearnViewController
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    int cateID = [[category_id objectAtIndex:indexPath.row] intValue];
    LearnViewController *svc =[self.storyboard instantiateViewControllerWithIdentifier:@"LearnViewController"];
    svc.selectedIndex = (NSInteger*)cateID;
    [self presentViewController: svc animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backaction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
