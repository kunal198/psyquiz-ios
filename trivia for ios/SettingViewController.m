//
//  ViewController.m
// Trivia for ios

#import "SettingViewController.h"
#import <AudioToolbox/AudioServices.h>
#import "ViewController.h"

//#define YOUR_APP_STORE_ID 984412105 //Change this one to your ID
//#define YOUR_APP_STORE_ID 893840569 //Change this one to your ID


@interface SettingViewController ()

@end

@implementation SettingViewController{
    NSUserDefaults *prefs;
    NSString *SoundStatus, *VibrateStatus;
}
@synthesize sound,vibrate,volume1,volvalue,SoundOutlet,VibratOutlet;
@synthesize lblBorder;


- (void)viewDidLoad {
    [super viewDidLoad];

    [[UIApplication sharedApplication] setStatusBarHidden:YES];

    
    _seprateLine.frame = CGRectMake(_shareMeBtn.frame.origin.x + _shareMeBtn.frame.size.width, _seprateLine.frame.origin.y, _seprateLine.frame.size.width, _seprateLine.frame.size.height);
    
    _shareMeBtn.layer.cornerRadius = _shareMeBtn.frame.size.height / 2;
    _shareMeBtn.layer.borderWidth = 1.0f;
    _shareMeBtn.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    _shareMeBtn.layer.masksToBounds = YES;
    
    
    
    
    _rateMeBtn.layer.cornerRadius = _rateMeBtn.frame.size.height / 2;
    _rateMeBtn.layer.borderWidth = 1.0f;
    _rateMeBtn.layer.borderColor = [[UIColor lightGrayColor]CGColor];;
    _rateMeBtn.layer.masksToBounds = YES;
    
    
    
    
    
    CALayer *layer = lblBorder.layer;
    layer.backgroundColor = [UIColor whiteColor];
    layer.borderColor=[[UIColor colorWithRed:255/255.0 green:179/255.0 blue:0/255.0 alpha:1] CGColor];
    layer.borderWidth = 1.0f;
    prefs = [NSUserDefaults standardUserDefaults];
    SoundStatus = [prefs stringForKey:@"S"];
    if (SoundStatus == nil) {
        // No value found
        SoundStatus = @"ON";
    }    
    VibrateStatus = [prefs stringForKey:@"V"];

    if (VibrateStatus == nil) {
        // No value found
        VibrateStatus = @"ON";
    }
    

    if([SoundStatus isEqualToString:@"ON"]){
        [SoundOutlet setOn:YES animated:YES];
    }
    else{
        [SoundOutlet setOn:NO animated:YES];
    }

    if([VibrateStatus isEqualToString:@"ON"]){
        [VibratOutlet setOn:YES animated:YES];
    } else{
        [VibratOutlet setOn:NO animated:YES];
    }
    
    self.view.backgroundColor = [[UIColor colorWithRed:(0/255.0) green:(138/255.0) blue:(214/255.0) alpha:1.0] colorWithAlphaComponent:0.7f ];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//Share Button Event
- (IBAction)ShareMe:(UIButton *)sender {
    
    NSString * appId = @"976930252";
    NSString * theUrl = [NSString  stringWithFormat:@"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=%@&onlyLatestVersion=true&pageNumber=0&sortOrdering=1&type=Purple+Software",appId];
    if ([[UIDevice currentDevice].systemVersion integerValue] > 6) theUrl = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/id%@",appId];
    
    NSString *texttoshare = [NSString stringWithFormat:@"Let me recommend you this application. Click to Download:  %@.",theUrl];
    NSString *sub = @"Trivia for ios";
    UIImage *imagetoshare = [UIImage imageNamed:@"ShareIcon.png"];
    NSArray *activityItems = @[sub, texttoshare, imagetoshare];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityVC.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentViewController:activityVC animated:TRUE completion:nil];
}

//Rate Me Button Event
- (IBAction)RateMe:(UIButton *)sender {
    //NSLog(@"Click on rate me");
    NSString * appId = @"976930252";
    NSString * theUrl = [NSString  stringWithFormat:@"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=%@&onlyLatestVersion=true&pageNumber=0&sortOrdering=1&type=Purple+Software",appId];
    if ([[UIDevice currentDevice].systemVersion integerValue] > 6) theUrl = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/id%@",appId];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:theUrl]];
}

- (IBAction)changeSound:(id)sender{
    SystemSoundID Soundidd;
    if(SoundOutlet.on){
        [prefs setObject:@"ON" forKey:@"S"];
        
        AudioServicesPlaySystemSound(Soundidd);

    } else{
        [prefs setObject:@"OFF" forKey:@"S"];
        AudioServicesDisposeSystemSoundID(Soundidd);

    }
}

//Vibrate setting
- (IBAction)changeVibration:(id)sender{

    if(VibratOutlet.on){
        [prefs setObject:@"ON" forKey:@"V"];
       AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
        
    } else{
        [prefs setObject:@"OFF" forKey:@"V"];
         AudioServicesDisposeSystemSoundID(kSystemSoundID_Vibrate);
    }
}

- (IBAction)BackHome:(UIButton *)sender {
//    ViewController *svc =[self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
//    [self presentViewController: svc animated:YES completion:nil];
}

- (IBAction)BACKACTION:(id)sender {
    
}

- (IBAction)backaction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)dealloc {
    [SoundOutlet release];
    [VibratOutlet release];
    [lblBorder release];
    [_shareMeBtn release];
    [_rateMeBtn release];
    [_seprateLine release];
    [super dealloc];
}
@end
