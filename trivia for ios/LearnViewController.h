//
//  ViewController.h
// Trivia for ios

#import <UIKit/UIKit.h>

@interface LearnViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextView *qustn;
@property (strong, nonatomic) IBOutlet UITextView *ans;
@property (strong, nonatomic) IBOutlet UILabel *levels;
@property (strong, nonatomic) IBOutlet UIButton *jump;
@property (strong, nonatomic) IBOutlet UIButton *answer;
@property (strong, nonatomic) NSMutableArray *qustns;
@property (nonatomic, assign) NSInteger *selectedIndex; // it will give selected row to next view controller
@property (retain, nonatomic) IBOutlet UIButton *showAnswerOutlet;
- (IBAction)Backbutton:(id)sender;

-(IBAction)nextQstn:(id)sender;
-(IBAction)preQstn:(id)sender;
- (IBAction)jumpButtonClicked:(id)sender;
- (IBAction)AnswerButtonClicked:(id)sender;
@end

