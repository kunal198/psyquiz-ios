//
//  ViewController.m
// Trivia for ios

#import "ViewController.h"
#import "CurrentAffairViewController.h"
#import "AppDelegate.h"
#import "SettingViewController.h"
#import "LevelViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <Social/Social.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "Function.h"
#import "staticstrings.h"
#import "InfoViewController.h"
#import "AppDelegate.h"
#import "Reachability.h"

@implementation ViewController{
    AppDelegate *session;
    NSUserDefaults *prefs;
    NSString *HowManyTimePlayQuiz,*CountQuestionCompleted,*CurrentAffairTotalScore,*CountRightAnswareQuestions,*CurrentAffairLevelCOmpleted,*LevelCompleted,*TotalScore;


}
@synthesize quiz,setting,btn1;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    _letsLearnView.layer.cornerRadius = _letsLearnView.frame.size.height / 2;
    _letsLearnView.layer.masksToBounds = YES;
    _quizPlayView.layer.cornerRadius = _quizPlayView.frame.size.height / 2;
    _quizPlayView.layer.masksToBounds = YES;
    _settingView.layer.cornerRadius = _settingView.frame.size.height / 2;
    _settingView.layer.masksToBounds = YES;
    _infoView.layer.cornerRadius = _infoView.frame.size.height / 2;
    _infoView.layer.masksToBounds = YES;
    
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnAllQuestion:)];
    [self.btn1 addGestureRecognizer:tap];
    
    prefs = [NSUserDefaults standardUserDefaults];
    HowManyTimePlayQuiz = [NSString stringWithFormat:@"%ld",(long)[prefs integerForKey:@"PlayQuizCount"]];
    CountQuestionCompleted = [NSString stringWithFormat:@"%ld",(long)[prefs integerForKey:@"TotalCompleteQuizQuestion"]];
    CurrentAffairTotalScore = [NSString stringWithFormat:@"%ld",(long)[prefs integerForKey:@"CurrentAffairTotalScore"]];
    CountRightAnswareQuestions = [NSString stringWithFormat:@"%ld",(long)[prefs integerForKey:@"TotalCurrectAnsQuizQuestion"]];
    CurrentAffairLevelCOmpleted = [NSString stringWithFormat:@"%ld",(long)[prefs integerForKey:@"CurrentAffairLevel"]];
    LevelCompleted = [NSString stringWithFormat:@"%ld",(long)[prefs integerForKey:@"QuizLevel"]];
    TotalScore = [NSString stringWithFormat:@"%ld",(long)[prefs integerForKey:@"TotalScore"]];
    session = [[UIApplication sharedApplication]delegate];
    
    NSArray *keys = [NSArray arrayWithObjects:@"how_many_time_play_quiz", @"count_question_completed",@"current_affair_totalScore",@"count_right_answare_questions",@"current_affair_levelCompleted",@"level_completed",@"total_score", nil];
    NSArray *objects = [NSArray arrayWithObjects:HowManyTimePlayQuiz,CountQuestionCompleted,CurrentAffairTotalScore,CountRightAnswareQuestions,CurrentAffairLevelCOmpleted,LevelCompleted,TotalScore, nil];
    
    NSDictionary *Score = [NSDictionary dictionaryWithObjects:objects
                                                           forKeys:keys];
    
    NSDictionary *SynchData = [[NSDictionary alloc]init];
    SynchData = [NSDictionary dictionaryWithObjectsAndKeys:Score,@"snapshotTemp",@"1.1",@"version", nil];

    NSError *error;
    session.SynchDataScore = [NSJSONSerialization dataWithJSONObject:SynchData
                                                       options:NSJSONWritingPrettyPrinted error:&error];
  
}

-(void) tapOnAllQuestion:(UIGestureRecognizer*) recognizer
{
            LevelViewController *svc =[self.storyboard instantiateViewControllerWithIdentifier:@"LevelViewController"];
        [self presentViewController: svc animated:YES completion:nil];
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)alert
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"firstly need to conect to interent for load data from server" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [alert show];

}

- (IBAction)infoBtn:(id)sender
{
    InfoViewController *infovc =[self.storyboard instantiateViewControllerWithIdentifier:@"InfoViewController"];
    [self presentViewController: infovc animated:YES completion:nil];
    
    
}

- (IBAction)PlayQuiz:(id)sender
{
    
    
        CurrentAffairViewController *svc =[self.storyboard instantiateViewControllerWithIdentifier:@"CurrentAffairViewController"];
        [self presentViewController: svc animated:YES completion:nil];
    
    
    
}


- (IBAction)SettingVC:(UIButton *)sender
{
    
            SettingViewController *svc =[self.storyboard instantiateViewControllerWithIdentifier:@"SettingViewController"];
        [self presentViewController: svc animated:YES completion:nil];

}



- (void)dealloc {
    [_infoView release];
    [_settingView release];
    [_quizPlayView release];
    [_letsLearnView release];
    [super dealloc];
}
@end
