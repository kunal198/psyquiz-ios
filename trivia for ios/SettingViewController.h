//
//  ViewController.h
// Trivia for ios

#import <UIKit/UIKit.h>

@interface SettingViewController : UIViewController

@property (nonatomic, strong) UISwitch *sound;
@property (nonatomic, strong) UISwitch *vibrate;
@property (nonatomic, strong) UISlider *volume1;
@property (nonatomic, strong) UITextField *volvalue;
@property (retain, nonatomic) IBOutlet UISwitch *SoundOutlet;
@property (retain, nonatomic) IBOutlet UISwitch *VibratOutlet;
@property (retain, nonatomic) IBOutlet UILabel *lblBorder;
@property (retain, nonatomic) IBOutlet UIButton *shareMeBtn;
@property (retain, nonatomic) IBOutlet UIButton *rateMeBtn;
@property (retain, nonatomic) IBOutlet UILabel *seprateLine;

- (IBAction)ShareMe:(UIButton *)sender;
- (IBAction)RateMe:(UIButton *)sender;
- (IBAction)changeSound:(id)sender;
- (IBAction)changeVibration:(id)sender;
- (IBAction)BackHome:(UIButton *)sender;
- (IBAction)BACKACTION:(id)sender;

- (IBAction)backaction:(id)sender;

@end

