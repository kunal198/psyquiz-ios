//
//  ViewController.h
// Trivia for ios

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
{
  
   
}
@property (strong, nonatomic) IBOutlet UIButton *btn1;
@property (strong, nonatomic) IBOutlet UIButton *quiz;
@property (strong, nonatomic) IBOutlet UIButton *setting;
@property (retain, nonatomic) IBOutlet UIView *infoView;
@property (retain, nonatomic) IBOutlet UIView *settingView;
@property (retain, nonatomic) IBOutlet UIView *quizPlayView;
@property (retain, nonatomic) IBOutlet UIView *letsLearnView;
@property (strong, nonatomic) NSMutableArray *qustns;
- (IBAction)infoBtn:(id)sender;

- (IBAction)PlayQuiz:(id)sender;
- (IBAction)SettingVC:(UIButton *)sender;
-(void)displayads;

@end

