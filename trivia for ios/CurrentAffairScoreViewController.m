//
//  CurrentAffairScoreViewController.m
// Trivia for ios
//

#import "CurrentAffairScoreViewController.h"
#import "LevelViewController.h"
#import "staticstrings.h"
#import "ViewController.h"

#import "AppDelegate.h"
#import "CurrentAffairViewController.h"

@class GPPMediaAttachment;
@class GPPSignIn;

@interface CurrentAffairScoreViewController ()

@end

@implementation CurrentAffairScoreViewController{
    NSUserDefaults *prefs;
    NSString *discription;
    AppDelegate *session;
     int nextStatus;
}
@synthesize totalScore,levelScore,lblFinish,infoRequest,btnPlayagain;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _riteScore.text = [NSString stringWithFormat:@"%@",_ritScore];
     _wrongScore.text = [NSString stringWithFormat:@"%@",_wrngScore];
    
    _homeButtonOutlet.layer.cornerRadius = _homeButtonOutlet.frame.size.height / 2;
    _homeButtonOutlet.layer.masksToBounds = YES;
    btnPlayagain.layer.cornerRadius = _tryAgainBtnOutlet.frame.size.height / 2;
    btnPlayagain.layer.masksToBounds = YES;
    
    
     session = [[UIApplication sharedApplication]delegate];
     session.quizCompleteStatus=@"0";
     int tcas = [[staticstrings selectedbutton] intValue];
     prefs = [NSUserDefaults standardUserDefaults];
     [prefs setInteger:tcas forKey:@"CurrentAffairTotalScore"];   // save total score of previous level

    
    
    
      totalScore.text = [NSString stringWithFormat:@"TOTAL SCORE: %@", [staticstrings selectedbutton]]; //total score
    //  [self.view addSubview:totalScore];
    
    
    NSLog(@"@%",[staticstrings levelScore]);
     levelScore.text=[NSString stringWithFormat:@"THIS LEVEL SCORE: %@", [staticstrings levelScore]]; //current level score
    // [self.view addSubview:levelScore];
    
    NSString *score = [staticstrings selectedlabel];
    
    //levelScore.text = [NSString stringWithFormat:@"Total Score: %@",score];
    
    int tls = [score intValue];
    int CurrentAffairLevel = (int)[prefs integerForKey:@"CurrentAffairLevel"];   //save current Level no.
    if(CurrentAffairLevel==0){
        CurrentAffairLevel =1;
}
    //if answer true>=5 then Play Next Level otherwise Play Again
        NSString *str1 = @"Level";
        NSString *str2 = @"Finished...";
        NSString *str3 = [NSString stringWithFormat:@"%@ %d %@", str1,CurrentAffairLevel,str2];
        self.lblFinish.text=str3;
    
    if(tls >= 7){
        
            NSString *str2 = @"Finished...";
            NSString *str3 = [NSString stringWithFormat:@"Level %d %@",CurrentAffairLevel ,str2];
            self.lblFinish.text=str3;
        
        [btnPlayagain setTitle:@"PLAY NEXT" forState:UIControlStateNormal];
        nextStatus = 0;
    }
    else
    {
        NSString *printstr = [NSString stringWithFormat:@"Level %d Not Completed plz try again.",CurrentAffairLevel];
        self.lblFinish.text = printstr;
        [btnPlayagain setTitle:@"TRY AGAIN" forState:UIControlStateNormal];
        _lvlCmpleteMsg.text = [NSString stringWithFormat:@"LEVEL INCOMPLETE"];
        nextStatus = 1;
    }

[prefs setInteger:CurrentAffairLevel forKey:@"CurrentAffairLevel"];

int CurrentLevel = (int)[prefs integerForKey:@"CurrentAffairLevel"];
    
    
    _homeButtonOutlet.layer.cornerRadius = _homeButtonOutlet.frame.size.height / 2;
    _homeButtonOutlet.layer.masksToBounds = YES;
    
    _homeButtonOutlet.layer.borderColor = [UIColor blackColor].CGColor;
    _homeButtonOutlet.layer.borderWidth = 1.0f;
    btnPlayagain.layer.cornerRadius = btnPlayagain.frame.size.height / 2;
    btnPlayagain.layer.borderColor = [UIColor blackColor].CGColor;
    btnPlayagain.layer.borderWidth = 1.0f;
    btnPlayagain.layer.masksToBounds = YES;
    
}


-(void)scoreLevelUP{
    NSString *levelstr = [staticstrings selectedlabel];
    
    int tls = [levelstr intValue];
    
    int CurrentAffairLevel = (int)[prefs integerForKey:@"CurrentAffairLevel"];
    if(tls >= 5){
        CurrentAffairLevel++;
        NSString *str2 = @"Finished...";
        NSString *str3 = [NSString stringWithFormat:@"%@ %@",session.CurrentLevel ,str2];
        self.lblFinish.text=str3;
    }
    else{
        NSString *printstr = [NSString stringWithFormat:@"%@ Not Completed plz try again.",session.CurrentLevel];
        self.lblFinish.text = printstr;
    }
    [prefs setInteger:CurrentAffairLevel forKey:@"CurrentAffairLevel"];
    
}

//Back to Home
- (IBAction)BackHome:(UIButton *)sender {
    ViewController *svc =[self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self presentViewController: svc animated:YES completion:nil];
}

//play again
- (IBAction)Replay:(UIButton *)sender {
    if(nextStatus == 0){
        [self scoreLevelUP];
    }
    CurrentAffairViewController *svc =[self.storyboard instantiateViewControllerWithIdentifier:@"CurrentAffairViewController"];
    [self presentViewController: svc animated:YES completion:nil];
}


//share total score
-(IBAction)Share:(id)sender
{
    int CurrentLevel = (int)[prefs integerForKey:@"CurrentAffairLevel"];
    int score = (int)[prefs integerForKey:@"CurrentAffairTotalScore"];
    discription = [NSString stringWithFormat:@"I'm just completed Level : %d on #%@ with %d Can you beat my high score?",CurrentLevel,[staticstrings appNameHashTag],score];
    UIImage *imagetoshare = [UIImage imageNamed:@"ShareIcon.png"];
    NSArray *activityItems = @[discription, imagetoshare];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityVC.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentViewController:activityVC animated:TRUE completion:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)dealloc {
    [_homeButtonOutlet release];
    [_tryAgainBtnOutlet release];
    [_lvlCmpleteMsg release];
    [_riteScore release];
    [_wrongScore release];
    [super dealloc];
}
@end
