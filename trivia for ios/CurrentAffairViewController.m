//
//  CurrentAffairViewController.m
// Trivia for ios

#import "CurrentAffairViewController.h"
#import "LearnViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <Social/Social.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "Function.h"
#import "staticstrings.h"
#import "AppDelegate.h"
#import "CurrentAffairScoreViewController.h"
#import "ViewController.h" 
#import <AudioToolbox/AudioServices.h>
#import <AVFoundation/AVAudioPlayer.h>
//#import <GooglePlayGames/GooglePlayGames.h>
#import <GoogleMobileAds/GADInterstitial.h>

//#import "AFNetworking.h"

@interface CurrentAffairViewController ()

@property(nonatomic, strong) GADInterstitial *interstitial;

@end

@implementation CurrentAffairViewController
{
    NSMutableArray *qustns;
    NSMutableArray *options;
    int scoretest;
    int scoreBeforeStartLevel;
    int right;
    int wrong;
    int totalScore;
    int qustnIndex1;
    int CountID;
    AppDelegate *session;
    NSUserDefaults *prefs;

}
int Ctotal1=0;
int CurrentAffairLevel;

@synthesize qustn1,optnA,optnB,optnC,optnD,score,total,level,lblright,lblwrong;
@synthesize infoRequest;

- (void)viewDidLoad
{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable)
    {
        NSLog(@"Valid Internet Connection");
        isInternetConnection = true;
        
    }
    else
    {
        isInternetConnection = false;
        
    }
    
    
    session = [[UIApplication sharedApplication]delegate];    
   // NSLog(@"Google Mobile Ads SDK version: %@", [GADRequest sdkVersion]);
    _correctview.layer.cornerRadius =15;
    _crossview.layer.cornerRadius = 15;
    optnA.layer.cornerRadius = optnA.frame.size.height / 2;
    optnA.layer.borderColor = [[UIColor grayColor ]CGColor];
    optnA.layer.borderWidth = 1.0f;
    optnA.layer.masksToBounds = YES;
    optnB.layer.cornerRadius = optnB.frame.size.height / 2;
    optnB.layer.borderColor = [[UIColor grayColor ]CGColor];
    optnB.layer.borderWidth = 1.0f;
    optnB.layer.borderColor = [[UIColor grayColor ]CGColor];
    optnB.layer.masksToBounds = YES;
    optnC.layer.cornerRadius = optnC.frame.size.height / 2;
    optnC.layer.borderWidth = 1.0f;
    optnC.layer.borderColor = [[UIColor grayColor ]CGColor];
    optnC.layer.masksToBounds = YES;
    optnD.layer.cornerRadius = optnD.frame.size.height / 2;
    optnD.layer.borderWidth = 1.0f;
    optnD.layer.borderColor = [[UIColor grayColor ]CGColor];
    optnD.layer.masksToBounds = YES;
    optnA.backgroundColor=[UIColor whiteColor];
    optnB.backgroundColor=[UIColor whiteColor];
    optnC.backgroundColor=[UIColor whiteColor];
    optnD.backgroundColor=[UIColor whiteColor];

    
    
    NSLog(@"optnA = %f",optnA.frame.size.height);
    NSLog(@"optnB = %f",optnB.frame.size.height);
    NSLog(@"optnC = %f",optnC.frame.size.height);
    NSLog(@"optnD = %f",optnD.frame.size.height);
    
    self.interstitial = [[GADInterstitial alloc] init];
    self.interstitial.adUnitID = [staticstrings adMobID];
    self.interstitial.delegate = self;
    
    GADRequest *request = [GADRequest request];
    // Requests test ads on test devices.
    //request.testDevices = @[@"2077ef9a63d2b398840261c8221a0c9b"];
    [self.interstitial loadRequest:request];
    session = [[UIApplication sharedApplication]delegate];
    prefs = [NSUserDefaults standardUserDefaults];
    scoreBeforeStartLevel = (int)[prefs integerForKey:@"CurrentAffairTotalScore"];   // get total score of previous level
    scoretest = scoreBeforeStartLevel;

    
if([session.CCompleteStatus isEqualToString:@"0"]){
    qustnIndex1 =0;
    [super viewDidLoad];
      CurrentAffairLevel = (int)[prefs integerForKey:@"CurrentAffairLevel"];   //current Level no.
      if(CurrentAffairLevel ==0){
        CurrentAffairLevel = 1;
    }
    
    self.qustns = [[NSMutableArray alloc]init];
    NSString *isPlayLocal = [staticstrings getIsLocalPlay];
    
    
    if ([isPlayLocal isEqualToString:@"true"])
    {
        self.qustns = [Function getAllTask];                   // Get database Local
    }else{
        self.qustns = [self getQuestions];// Get database from server
        _questions = self.qustns;
        [self savedataLocally];
    }
    
    
    if (self.qustns.count == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Network Error" message:@"Make sure you are connected to internet" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];

    }
    else
    {
        NSString *question = [[self.qustns objectAtIndex:qustnIndex1] objectForKey:@"question"];
        scoretest = (int)[prefs integerForKey:@"CurrentAffairTotalScore"];
        wrong=0;
        right=0;
    }
    
    
    
}
else if([session.CCompleteStatus isEqualToString:@"1"]){
    qustnIndex1 = [session.CQueCount intValue];
    self.qustns = session.CCurrentArray;
    scoretest = [session.CtotalScore intValue];
    wrong = [session.Cwrongque intValue];
    right = [session.CrightQue intValue];
}
    int Ctotal1 = [self.qustns count];
    lblwrong.text = [NSString stringWithFormat:@"%d",wrong];
    lblright.text = [NSString stringWithFormat:@"%d",right];
    
    //UI Design
    //UIFont* boldFont = [UIFont boldSystemFontOfSize:[UIFont systemFontSize]];
    //[score setTextColor:[UIColor colorWithRed:0.0/255.0 green:100.0/255.0 blue:0.0/255.0 alpha:1.0]];
   // [score setFont:boldFont];
    score.text = [NSString stringWithFormat:@"%d",scoretest];

    NSString *strlevel = [NSString stringWithFormat:@"LEVEL-%d",CurrentAffairLevel ];
    level.text = strlevel;
    
//    qustn1.text = [[self.qustns objectAtIndex:qustnIndex1] objectForKey:@"question"];
   // [total setFont:boldFont];
    self.total.text = [NSString stringWithFormat:@"%d /%d",qustnIndex1+1,Ctotal1];
    
//    CGRect frameRect = score.frame;
//    frameRect.size.height = 35;
//    score.frame = frameRect;
//    frameRect.size.width = 50;
//    score.frame = frameRect;
//    frameRect.size.height = 35;
//    total.frame = frameRect;
//    frameRect.size.width = 45;
//    total.frame = frameRect;
    score.layer.cornerRadius = 20.0f;
    total.layer.cornerRadius = 20.0f;

    qustn1.backgroundColor = [UIColor whiteColor];
    //[[self.qustn1 layer] setBorderColor:[[UIColor colorWithRed:255/255.0 green:179/255.0 blue:0/255.0 alpha:1] CGColor]];
   // [[self.qustn1 layer] setBorderWidth:1.0];
    //[[self.qustn1 layer] setCornerRadius:15];
    [self ReloadOption];
    self.qustn1.textAlignment = NSTextAlignmentCenter;
    [self ReloadOption];
    [self NextQustn];
    
    
    
}

//Get Database from serevr



-(void)questinList
{
    
//    if (_questions.count == 0)
//    {
//        <#statements#>
//    }
    
}








-(NSMutableArray*)getQuestions
{
    NSString *questionurl = [NSString stringWithFormat:@"%@%d",[staticstrings getquestionwithoptionbylevelid],CurrentAffairLevel ];
  
    NSURL *url1 = [NSURL URLWithString:questionurl];
    NSError *error1 = nil;
    NSStringEncoding encoding1;
    NSString *jsonreturn1 = [[NSString alloc] initWithContentsOfURL:url1 usedEncoding:&encoding1 error:&error1];
    
    if (!jsonreturn1)
    {
        UIAlertView *alertView1 = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                             message:@"There is no server response please try again later."
                                                            delegate:self
                                                   cancelButtonTitle:nil
                                                   otherButtonTitles:@"OK" ,nil];
        [alertView1 show];
        [alertView1 release];
        
        
        return nil;
        
        
    }
    else
    {
      
        
        NSMutableArray *dict1 = [jsonreturn1 JSONValue];
        
        NSLog(@"dict1 %@",dict1);
        
        return dict1;

     
        
        
    }
    
}



-(void)savedataLocally
{
    
    for (int i = 0;  i <_questions.count; i++)
    {
        [DBOperation insertquestion:[[_questions objectAtIndex:i] valueForKey:@"question" ] optionA:[[_questions objectAtIndex:i] valueForKey:@"optiona" ] optionB:[[_questions objectAtIndex:i] valueForKey:@"optionb" ] optionC:[[_questions objectAtIndex:i] valueForKey:@"optionc" ] optionD:[[_questions objectAtIndex:i] valueForKey:@"optiond" ] RightAns:[[_questions objectAtIndex:i] valueForKey:@"quizlevel" ] quiz_level:[[_questions objectAtIndex:i] valueForKey:@"rightans" ]];
    }
    
    
    
    
    
//    if (<#condition#>) {
//        <#statements#>
//    }
    
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//UI of Button
-(void)ReloadOption{
    //optnA.backgroundColor = [[UIColor colorWithRed:(255/255.0) green:(179/255.0) blue:(0/255.0) alpha:1.0] colorWithAlphaComponent:0.7f ];
   // optnA.layer.borderColor = [[UIColor colorWithRed:255/255.0 green:179/255.0 blue:0/255.0 alpha:1] CGColor];
    optnA.layer.borderWidth = 0.5f;
    optnA.layer.cornerRadius = 15.0f;
    //optnB.backgroundColor = [[UIColor colorWithRed:(255/255.0) green:(179/255.0) blue:(0/255.0) alpha:1.0] colorWithAlphaComponent:0.7f ];
    //optnB.layer.borderColor = [[UIColor colorWithRed:255/255.0 green:179/255.0 blue:0/255.0 alpha:1] CGColor];
    optnB.layer.borderWidth = 0.5f;
    optnB.layer.cornerRadius = 15.0f;
    //optnC.backgroundColor = [[UIColor colorWithRed:(255/255.0) green:(179/255.0) blue:(0/255.0) alpha:1.0] colorWithAlphaComponent:0.7f ];
    //optnC.layer.borderColor = [[UIColor colorWithRed:255/255.0 green:179/255.0 blue:0/255.0 alpha:1] CGColor];
    optnC.layer.borderWidth = 0.5f;
    optnC.layer.cornerRadius = 15.0f;
   // optnD.backgroundColor = [[UIColor colorWithRed:(255/255.0) green:(179/255.0) blue:(0/255.0) alpha:1.0] colorWithAlphaComponent:0.7f ];
    //optnD.layer.borderColor = [[UIColor colorWithRed:255/255.0 green:179/255.0 blue:0/255.0 alpha:1] CGColor];
    optnD.layer.borderWidth = 0.5f;
    optnD.layer.cornerRadius = 15.0f;
    NSLog(@"optnA = %f",optnA.frame.size.height);
    NSLog(@"optnB = %f",optnB.frame.origin.x);
    NSLog(@"optnC = %f",optnC.frame.size.height);
    NSLog(@"optnD = %f",optnD.frame.origin.x);
}

//Display Next Question
-(void)NextQustn
{
    optnA.backgroundColor = [UIColor whiteColor];
    optnB.backgroundColor = [UIColor whiteColor];
    optnC.backgroundColor = [UIColor whiteColor];
    optnD.backgroundColor = [UIColor whiteColor];
    
    
    [self ReloadOption];
    [self OptionButtonEnable];
    if(qustnIndex1 < [self.qustns count])
    {
        NSString *isPlayLocal = [staticstrings getIsLocalPlay];
        options = [NSMutableArray array];
        if ([isPlayLocal isEqualToString:@"true"]) {
            [options addObject:[[self.qustns objectAtIndex:qustnIndex1] objectForKey:@"OptionA"]];
            [options addObject:[[self.qustns objectAtIndex:qustnIndex1] objectForKey:@"OptionB"]];
            [options addObject:[[self.qustns objectAtIndex:qustnIndex1] objectForKey:@"OptionC"]];
            [options addObject:[[self.qustns objectAtIndex:qustnIndex1] objectForKey:@"OptionD"]];
        }else{
            [options addObject:[[self.qustns objectAtIndex:qustnIndex1] objectForKey:@"optiona"]];
            [options addObject:[[self.qustns objectAtIndex:qustnIndex1] objectForKey:@"optionb"]];
            [options addObject:[[self.qustns objectAtIndex:qustnIndex1] objectForKey:@"optionc"]];
            [options addObject:[[self.qustns objectAtIndex:qustnIndex1] objectForKey:@"optiond"]];
        }
        
        NSMutableArray *shuffle = [[NSMutableArray alloc] initWithCapacity:[options count]];
        while ([options count])
        {
            int index = arc4random()%[options count];
            [shuffle addObject:[options objectAtIndex:index]];
            [options removeObjectAtIndex:index];
        }
        for (int i=0; i<[shuffle count]; i++)
            
           options = shuffle;
        
        [optnA setTitle: [shuffle objectAtIndex:0] forState:UIControlStateNormal];
        
        [optnB setTitle: [shuffle objectAtIndex:1] forState:UIControlStateNormal];
        
        [optnC setTitle: [shuffle objectAtIndex:2] forState:UIControlStateNormal];
        
        [optnD setTitle: [shuffle objectAtIndex:3] forState:UIControlStateNormal];
        
        int Ctotal1 = [self.qustns count];
        
        qustn1.text = [[self.qustns objectAtIndex:qustnIndex1] objectForKey:@"question"];
        qustnIndex1++;
        self.total.text = [NSString stringWithFormat:@"%d /%d",qustnIndex1,Ctotal1];
     
    }else{
        if ([self.interstitial isReady]) {
            [self.interstitial presentFromRootViewController:self];
        }else{
            [staticstrings setselectedbutton:score.text];
            [staticstrings setselectedlabel:lblright.text];
            int levelScore = scoretest - scoreBeforeStartLevel;
            NSString *levelScoreStr = [NSString stringWithFormat:@"%d ",levelScore];
            [staticstrings setlevelScore:levelScoreStr ];
            CurrentAffairScoreViewController *svc =[self.storyboard instantiateViewControllerWithIdentifier:@"CurrentAffairScoreViewController"];
            svc.riteScore = lblright.text;
            svc.wrngScore = lblwrong.text;
            
            [self presentViewController: svc animated:YES completion:nil];
        }
    }
    
}

- (void)interstitialDidDismissScreen:(GADInterstitial *)interstitial {
    [staticstrings setselectedbutton:score.text];
    [staticstrings setselectedlabel:lblright.text];
    int levelScore = scoretest - scoreBeforeStartLevel;
    NSString *levelScoreStr = [NSString stringWithFormat:@"%d ",levelScore];
    [staticstrings setlevelScore:levelScoreStr ];
    
    CurrentAffairScoreViewController *svc =[self.storyboard instantiateViewControllerWithIdentifier:@"CurrentAffairScoreViewController"];
    svc.riteScore = lblright.text;
    svc.wrngScore = lblwrong.text;
    [self presentViewController: svc animated:YES completion:nil];
}

- (void)flashOff:(UIView *)v
{
    [UIView animateWithDuration:.05 delay:.50 options:UIViewAnimationOptionAllowUserInteraction animations:^ {
        v.alpha = .01;  //don't animate alpha to 0, otherwise you won't be able to interact with it
    } completion:^(BOOL finished) {
        [self flashOn:v];
    }];
}

- (void)flashOn:(UIView *)v
{
    [UIView animateWithDuration:.05 delay:.30 options:UIViewAnimationOptionAllowUserInteraction animations:^ {
        v.alpha = 1;
    } completion:^(BOOL finished) {
        if(CountID < 2)
        {
            CountID++;
            [self flashOff:v];
        }
        else
        {
            
            [self NextQustn];
            
        }
    }];
}

-(void)OptionButtonEnable{
    optnA.enabled=YES;
    optnB.enabled=YES;
    optnC.enabled=YES;
    optnD.enabled=YES;
}
-(void)OptionButtonDisable{
    optnA.enabled=NO;
    optnB.enabled=NO;
    optnC.enabled=NO;
    optnD.enabled=NO;
}
-(void)BlinkA{
    CALayer *layer = optnA.layer;
    layer.backgroundColor = [[UIColor colorWithRed:35.0/255.0 green:191.0/255.0 blue:0.0/255.0 alpha:0.9] CGColor];
    layer.borderColor = [[UIColor blackColor] CGColor];
    layer.cornerRadius = 15.0f;
    layer.borderWidth = 0.5f;
    [self flashOn:optnA];
}
-(void)BlinkB{
    CALayer *layer = optnB.layer;
    layer.backgroundColor = [[UIColor colorWithRed:35.0/255.0 green:191.0/255.0 blue:0.0/255.0 alpha:0.9] CGColor];
    layer.borderColor = [[UIColor blackColor] CGColor];
    layer.cornerRadius = 15.0f;
    layer.borderWidth = 0.5f;
    [self flashOn:optnB];
}
-(void)BlinkC{
    
    CALayer *layer = optnC.layer;
    layer.backgroundColor = [[UIColor colorWithRed:35.0/255.0 green:191.0/255.0 blue:0.0/255.0 alpha:0.9] CGColor];
    layer.borderColor = [[UIColor blackColor] CGColor];
    layer.cornerRadius = 15.0f;
    layer.borderWidth = 0.5f;
    
    [self flashOn:optnC];
}
-(void)BlinkD{
    CALayer *layer = optnD.layer;
    layer.backgroundColor = [[UIColor colorWithRed:35.0/255.0 green:191.0/255.0 blue:0.0/255.0 alpha:0.9] CGColor];
    layer.borderColor = [[UIColor blackColor] CGColor];
    layer.cornerRadius = 15.0f;
    layer.borderWidth = 0.5f;
    
    [self flashOn:optnD];
}

-(void)PlaySound: (NSString*)SoundName :(NSString*)an{
    NSString *soundPath = [[NSBundle mainBundle] pathForResource:SoundName ofType:an];
    NSURL *soundUrl = [NSURL fileURLWithPath:soundPath];
    AVAudioPlayer *player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
    [player play];
}

// Check True Answer
-(void) CheckAnswer: (int) btnIndex
{
    [self OptionButtonDisable];
    NSString *vibrateStatus = [prefs stringForKey:@"V"];
    NSString *soundStatus = [prefs stringForKey:@"S"];
    CountID=0;
    int answareQuestionIndex = qustnIndex1;
    answareQuestionIndex--;
    NSString *ansText =nil;
    NSString *isPlayLocal = [staticstrings getIsLocalPlay];
    NSString *GetRightAnsText;
    if ([isPlayLocal isEqualToString:@"true"]) {
         ansText = [[self.qustns objectAtIndex:answareQuestionIndex] objectForKey:@"RightAns"];
        if([ansText isEqualToString:@"A"]){
            GetRightAnsText = @"OptionA";
        }else if([ansText isEqualToString:@"B"]){
            GetRightAnsText = @"OptionB";
        }else if([ansText isEqualToString:@"C"]){
            GetRightAnsText = @"optionC";
        }else if([ansText isEqualToString:@"D"]){
            GetRightAnsText = @"optionD";
        }
    }else{
         ansText = [[self.qustns objectAtIndex:answareQuestionIndex] objectForKey:@"rightans"];
        if([ansText isEqualToString:@"A"]){
            GetRightAnsText = @"optiona";
        }else if([ansText isEqualToString:@"B"]){
            GetRightAnsText = @"optionb";
        }else if([ansText isEqualToString:@"C"]){
            GetRightAnsText = @"optionc";
        }else if([ansText isEqualToString:@"D"]){
            GetRightAnsText = @"optiona";
        }
    }

    NSString *answer = [[self.qustns objectAtIndex:answareQuestionIndex] objectForKey:GetRightAnsText];
    NSString *userAnsware=@"";
    if(btnIndex==1){
        userAnsware = optnA.currentTitle;
        if(userAnsware==answer)
        {
            right++;
            lblright.text=[NSString stringWithFormat:@"%d",right];
            scoretest =  scoretest + 10;
            score.text = [NSString stringWithFormat:@"%d",scoretest];
            [self BlinkA];
            if([soundStatus isEqualToString:@"ON"]){
                [self PlaySound:@"Right_ans" :@"wav"];
            }
        }
        else
        {
            scoretest =  scoretest-5;
            score.text = [NSString stringWithFormat:@"%d",scoretest];
            optnA.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:23.0/255.0 blue:66.0/255.0 alpha:1.0];
            [[optnA layer] setBorderWidth:0.5f];
           // [[optnA layer] setBorderColor:[UIColor yellowColor].CGColor];
            if([optnB.currentTitle isEqualToString:answer]){
                [self BlinkB];
            }else if([optnC.currentTitle isEqualToString:answer]){
                [self BlinkC];
            }
            else if([optnD.currentTitle isEqualToString:answer]){
                [self BlinkD];
            }
            if([vibrateStatus isEqualToString:@"ON"]){
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
            }
            if([soundStatus isEqualToString:@"ON"]){
                [self PlaySound:@"Wrong_ans" :@"wav"];
            }
            wrong++;
            lblwrong.text=[NSString stringWithFormat:@"%d",wrong];
        }
        
    }else if(btnIndex == 2){
        userAnsware = optnB.currentTitle;
        if(userAnsware==answer)
        {
            right++;
            lblright.text=[NSString stringWithFormat:@"%d",right];
            scoretest =  scoretest+10;
            score.text = [NSString stringWithFormat:@"%d",scoretest];
                      [self BlinkB];
            if([soundStatus isEqualToString:@"ON"]){
                [self PlaySound:@"Right_ans" :@"wav"];
            }
        }
        else
        {
                scoretest =  scoretest-5;
            score.text = [NSString stringWithFormat:@"%d",scoretest];
            wrong++;
            optnB.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:23.0/255.0 blue:66.0/255.0 alpha:1.0];
            [[optnB layer] setBorderWidth:0.5f];
            //[[optnB layer] setBorderColor:[UIColor yellowColor].CGColor];
            
            if([optnA.currentTitle isEqualToString:answer]){
                [self BlinkA];
            }else if([optnC.currentTitle isEqualToString:answer]){
                [self BlinkC];
            }
            else if([optnD.currentTitle isEqualToString:answer]){
                [self BlinkD];
            }
            if([vibrateStatus isEqualToString:@"ON"]){
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
            }
            if([soundStatus isEqualToString:@"ON"]){
                [self PlaySound:@"Wrong_ans" :@"wav"];
            }
            lblwrong.text=[NSString stringWithFormat:@"%d",wrong];
        }
        
    }else if(btnIndex ==3){
        userAnsware = optnC.currentTitle;
        if(userAnsware==answer)
        {
                       right++;
            lblright.text=[NSString stringWithFormat:@"%d",right];
            
            scoretest =  scoretest+10;
                      [self BlinkC];
            score.text = [NSString stringWithFormat:@"%d",scoretest];
            if([soundStatus isEqualToString:@"ON"]){
                [self PlaySound:@"Right_ans" :@"wav"];
            }
        }
        else
        {
                        scoretest =  scoretest-5;
            score.text = [NSString stringWithFormat:@"%d",scoretest];
            optnC.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:23.0/255.0 blue:66.0/255.0 alpha:1.0];
            [[optnC layer] setBorderWidth:0.5f];
           // [[optnC layer] setBorderColor:[UIColor yellowColor].CGColor];
            
            if([optnB.currentTitle isEqualToString:answer]){
                [self BlinkB];
            }else if([optnA.currentTitle isEqualToString:answer]){
                [self BlinkA];
            }
            else if([optnD.currentTitle isEqualToString:answer]){
                [self BlinkD];
            }
            if([vibrateStatus isEqualToString:@"ON"]){
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
            }
            if([soundStatus isEqualToString:@"ON"]){
                [self PlaySound:@"Wrong_ans" :@"wav"];
            }
            wrong++;
            lblwrong.text=[NSString stringWithFormat:@"%d",wrong];
        }
    }
    else
    {
        userAnsware = optnD.currentTitle;
        if(userAnsware==answer)
        {
                       right++;
            lblright.text=[NSString stringWithFormat:@"%d",right];     
            scoretest =  scoretest+10;
            score.text = [NSString stringWithFormat:@"%d",scoretest];
            [self BlinkD];
            if([soundStatus isEqualToString:@"ON"]){
                [self PlaySound:@"Right_ans" :@"wav"];
            }
        }
        else
        {
           
            scoretest =  scoretest-5;
            score.text = [NSString stringWithFormat:@"%d",scoretest];
            wrong++;
            optnD.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:23.0/255.0 blue:66.0/255.0 alpha:1.0];
            [[optnD layer] setBorderWidth:0.5f];
           // [[optnD layer] setBorderColor:[UIColor yellowColor].CGColor];
            
            if([optnB.currentTitle isEqualToString:answer]){
                [self BlinkB];
            }else if([optnC.currentTitle isEqualToString:answer]){
                [self BlinkC];
            }
            else if([optnA.currentTitle isEqualToString:answer]){
                [self BlinkA];
            }
            if([vibrateStatus isEqualToString:@"ON"]){
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
            }
            if([soundStatus isEqualToString:@"ON"]){
                [self PlaySound:@"Wrong_ans" :@"wav"];
            }
            lblwrong.text=[NSString stringWithFormat:@"%d",wrong];
        }

    }

}


-(IBAction)OptionA:(id)sender
{
    [self CheckAnswer:1];
    }
-(IBAction)OptionB:(id)sender
{
    [self CheckAnswer:2];
    }
-(IBAction)OptionC:(id)sender
{
    [self CheckAnswer:3];
  }
-(IBAction)OptionD:(id)sender
{
    [self CheckAnswer:4];
   }

//Pass Score Detail to CurrentAffairScoreViewController
- (void)passDataForward
{
    [staticstrings setselectedbutton:score.text];
    [staticstrings setselectedlabel:lblright.text];
    int levelScore = scoretest - scoreBeforeStartLevel;
    NSString *levelScoreStr = [NSString stringWithFormat:@"%d ",levelScore];
    [staticstrings setlevelScore:levelScoreStr ];
    CurrentAffairScoreViewController *svc =[self.storyboard instantiateViewControllerWithIdentifier:@"CurrentAffairScoreViewController"];
    svc.riteScore = lblright.text;
    svc.wrngScore = lblwrong.text;
    [self.navigationController pushViewController:svc animated:YES];
}

- (void)dataFromController:(NSString *)data
{
    score.text = data;
    lblright.text =data;
}

// Back to Home
- (IBAction)backbuttonaction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)BackHome:(UIButton *)sender {
    session.CCompleteStatus = @"1";
    session.CCurrentArray = self.qustns;
    session.CtotalScore = score.text;
    session.CrightQue = lblright.text;
    session.Cwrongque = lblwrong.text;
    session.CQueCount = [NSString stringWithFormat:@"%d",qustnIndex1-1];
    ViewController *svc =[self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self presentViewController: svc animated:YES completion:nil];
}
- (void)dealloc {
    [_rightAnswerCountView release];
    [_crossview release];
    [_correctview release];
    [score release];
    [total release];
    [level release];
    [super dealloc];
}
@end
