//
//  CurrentAffairViewController.h
// Trivia for ios


#import <UIKit/UIKit.h>
#import "Reachability.h"

@interface CurrentAffairViewController : UIViewController<NSURLConnectionDataDelegate,NSURLConnectionDelegate>
{
    NSMutableArray *dataObj;
    BOOL isInternetConnection;
}
@property (retain, nonatomic) IBOutlet UILabel *total;

@property (retain, nonatomic) IBOutlet UILabel *score;
@property (strong, nonatomic) IBOutlet UITextView *qustn1;
@property (strong, nonatomic) IBOutlet UIButton *optnA;
@property (strong, nonatomic) IBOutlet UIButton *optnB;
@property (strong, nonatomic) IBOutlet UIButton *optnC;
@property (strong, nonatomic) IBOutlet UIButton *optnD;
@property (strong, nonatomic) IBOutlet UILabel *lblright;
@property (strong, nonatomic) IBOutlet UILabel *lblwrong;
//@property (strong, nonatomic) IBOutlet UITextField *score;
//@property (strong, nonatomic) IBOutlet UITextField *total;
//@property (strong, nonatomic) IBOutlet UILabel *level;
@property (retain, nonatomic) IBOutlet UILabel *level;

@property (strong, nonatomic) NSMutableArray *qustns;
@property (strong, nonatomic) NSMutableArray *questions;
@property(nonatomic,strong) id infoRequest;
@property (retain, nonatomic) IBOutlet UIView *rightAnswerCountView;
- (IBAction)backbuttonaction:(id)sender;
@property (retain, nonatomic) IBOutlet UIView *correctview;

@property (retain, nonatomic) IBOutlet UIView *crossview;
- (IBAction)BackHome:(UIButton *)sender;

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender;

@end
