//
//  staticstrings.h
// Trivia for ios

#import <Foundation/Foundation.h>


@interface staticstrings : NSObject {
	
}

/*============================
 ==============================
 ============================*/
+ (NSString*)selectedbutton;
+ (NSString*)selectedlabel;
+ (NSString*)levelScore;
+ (NSString*)appNameStr;
+ (NSString*)appNameHashTag;
+ (NSString*)getquestionwithoptionbylevelid;
+ (NSString*)adMobID;
+ (NSString*)getquestionrightanswerbylevelid;
+(NSString*)getLevel;
+(NSString*)getIsLocalPlay;

+ (void)setselectedbutton:(NSString*)newselectedbutton;
+ (void)setselectedlabel:(NSString*)newselectedlabel;
+ (void)setlevelScore:(NSString*)newlevelScore;


@end
