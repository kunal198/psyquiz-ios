//
//  DBOperation.m
// Trivia for ios

#import "DBOperation.h"
#import "Function.h"
static sqlite3 *database = nil;
static int conn;
@implementation DBOperation

+(void)checkCreateDB{
	NSString *dbPath,*databaseName;
	databaseName=@"database.db";      //DataBase name
	NSArray *docPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
	NSString *docDir = [docPaths objectAtIndex:0];
	dbPath = [docDir stringByAppendingPathComponent:databaseName];
	BOOL success;
	NSFileManager *fm = [NSFileManager defaultManager];
	success=[fm fileExistsAtPath:dbPath];
	if(success){
		[self OpenDatabase:dbPath];
		return;
	}
	NSString *dbPathFromApp=[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:databaseName];
	[fm copyItemAtPath:dbPathFromApp toPath:dbPath error:nil];
	[self OpenDatabase:dbPath];
	
}
//Open database
+ (void) OpenDatabase:(NSString*)path
{
	@try
 {
	conn = sqlite3_open([path UTF8String], &database);
	if (conn == SQLITE_OK) {
			}
	else
		sqlite3_close(database); //Even though the open call failed, close the database connection to release all the memory.
 }	
	@catch (NSException *e) {
	}	
}
+(NSMutableArray*) selectData:(NSString *)sql{
	if ( conn == SQLITE_OK) {
		//NSLog(@"DBManagerObj::selectData \n");
		sqlite3_stmt *stmt = nil;
		if(sqlite3_prepare_v2(database, [sql UTF8String], -1, &stmt, NULL) != SQLITE_OK) {
			[NSException raise:@"DatabaseException" format:@"Error while creating statement. '%s'", sqlite3_errmsg(database)];
		}
		NSMutableArray *obj = [[NSMutableArray alloc]init] ;
		int numResultColumns = 0;
		while (sqlite3_step(stmt) == SQLITE_ROW) {
			numResultColumns = sqlite3_column_count(stmt);
            NSMutableDictionary *tmpObj = [[NSMutableDictionary alloc]init];
			for(int i = 0; i < numResultColumns; i++){
				if(sqlite3_column_type(stmt, i) == SQLITE_INTEGER){
					
					const char *name = sqlite3_column_name(stmt, i);
					NSString *columnName = [[NSString alloc]initWithCString:name encoding:NSUTF8StringEncoding];
					[tmpObj setObject:[NSString stringWithFormat:@"%i",sqlite3_column_int(stmt, i)] forKey:columnName];
					
					
				} else if (sqlite3_column_type(stmt, i) == SQLITE_FLOAT) {
					
					const char *name = sqlite3_column_name(stmt, i);
					NSString *columnName = [[NSString alloc]initWithCString:name encoding:NSUTF8StringEncoding];
									[tmpObj setObject:[NSString stringWithFormat:@"%f",sqlite3_column_double(stmt, i)] forKey:columnName];
					
				} else if (sqlite3_column_type(stmt, i) == SQLITE_TEXT) {
					const char *name = sqlite3_column_name(stmt, i);
					NSString *tmpStr = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, i)];
					if ( tmpStr == nil) {
						tmpStr = @"";
					}
					NSString *columnName = [[NSString alloc]initWithCString:name encoding:NSUTF8StringEncoding];
					[tmpObj setObject:tmpStr forKey:columnName];
					
					
				} else if (sqlite3_column_type(stmt, i) == SQLITE_BLOB) {
					
            }
				
        }
			[obj addObject:tmpObj];

		}
		return obj;
	} else {
		return nil;
	}
    
    
    
    
}


+(BOOL)InsertLevel:(NSString*)level_name levelId:(NSString*)levelId
{
    
    NSArray *arr  = [Function getlevel:levelId];
    
    if (arr.count)
    {
        sqlite3_stmt    *statement;
        NSString *insertSQL = [NSString stringWithFormat:
                               @"INSERT INTO quiz_level (level_id, level_name) VALUES (\"%@\", \"%@\" )",
                               levelId, level_name];
        
        //  TraceF(@"insertSQL::%@",insertSQL);
        
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_prepare_v2(database, insert_stmt,
                           -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            return 1;
        }//end if (sqlite3_step(statement)
        else {
            return NO;
        }

    }
    else
    {
        return true;
    }
    
    
}

+(BOOL)insertquestion:(NSString*)question optionA:(NSString*)optionA optionB:(NSString*)optionB optionC:(NSString*)optionC optionD:(NSString*)optionD RightAns:(NSString*)RightAns
quiz_level:(NSString*)quiz_level
{
    
   NSArray *arr = [Function getQuestion:question];
    
    if (arr.count == 0)
    {
        sqlite3_stmt    *statement;
        NSString *insertSQL = [NSString stringWithFormat:
                               @"INSERT INTO questions (question, optionA ,optionB,optionC,optionD,RightAns,quiz_level) VALUES (\"%@\", \"%@\" ,\"%@\", \"%@\", \"%@\", \"%@\" ,\"%@\")",
                               question, optionA,optionB,optionC,optionD,RightAns,quiz_level];
        
        //  TraceF(@"insertSQL::%@",insertSQL);
        
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_prepare_v2(database, insert_stmt,
                           -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            return 1;
        }//end if (sqlite3_step(statement)
        else {
            return NO;
        }

    }
    else
    {
        return  true;
    }
    
    
}


+(int) getLastInsertId{
	return sqlite3_last_insert_rowid(database);
}



//Save data at application closing time
+ (void) finalizeStatements {
	if(database) sqlite3_close(database);
}
@end
