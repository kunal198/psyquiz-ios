//
//  DBOperation.h
// Trivia for ios

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface DBOperation : NSObject
{

}
+(void)OpenDatabase:(NSString*)path;  //Open the Database
+(void)finalizeStatements;//Closing and do the final statement at application exits
+(void)checkCreateDB;
+(int) getLastInsertId;
+(BOOL) executeSQL:(NSString *)sqlTmp;
+(NSMutableArray*) selectData:(NSString *)sql;
+(BOOL)insertquestion:(NSString*)question optionA:(NSString*)optionA optionB:(NSString*)optionB optionC:(NSString*)optionC optionD:(NSString*)optionD RightAns:(NSString*)RightAns
           quiz_level:(NSString*)quiz_level;
+(BOOL)InsertLevel:(NSString*)level_name levelId:(NSString*)levelId;

@end
