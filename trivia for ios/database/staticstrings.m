//
//  staticstrings.m
// Trivia for ios

#import "staticstrings.h"
#import "Reachability.h"
@implementation staticstrings

/*===================================
 ====================================
 ==================================*/

static NSString *selectedbutton = nil;
static NSString *selectedlabel= nil;
static NSString *levelScore =nil;


// Replace you App Name Hash Tag here when sahre
static NSString *appNameHashTag=@"triviaforios";

// Replace you App Name here
static NSString *appNameStr=@"Trivia for ios";

// Replace you Admob Unit - id here
static NSString *adMobID=@"ca-app-pub-1891613648627380/3946587757";

//set you service URL plz don't foget otherwise it will be get our question database. For Four Option Question
static NSString *getquestionwithoptionbylevelid=@"http://beta.brstdev.com/codecanyan/getquestionwithoptionbylevelid.php?levelid=";

//set you service URL plz don't foget otherwise it will be get our question database. FOR ONE option Questions
static NSString *getquestionrightanswerbylevelid=@"http://beta.brstdev.com/codecanyan/getquestionrightanswerbylevelid.php?levelid=";

//Replace you Level here
static NSString *getLevel=@"http://beta.brstdev.com/codecanyan/getleveliinfo.php";

/*If we set question bank from local Assets folder SQlite file then it will be false if
we need question bank from web that is given PHP admin panel then set here true*/
static NSString *getIsLocalPlay=@"true";


+ (NSString*)selectedbutton {
	return selectedbutton;
}

+ (void)setselectedbutton:(NSString*)newselectedbutton {
	if(selectedbutton != newselectedbutton) {
		[selectedbutton release];
		selectedbutton = [newselectedbutton retain];
	}
}

+ (NSString*)selectedlabel {
    return selectedlabel;
}
+ (void)setselectedlabel:(NSString*)newselectedlabel {
    if(selectedlabel != newselectedlabel) {
        [selectedlabel release];
        selectedlabel = [newselectedlabel retain];
    }
}

+ (NSString*)levelScore {
    return levelScore;
}
+ (void)setlevelScore:(NSString*)newlevelScore {
    if(levelScore != newlevelScore) {
        [levelScore release];
        levelScore = [newlevelScore retain];
    }
}

+ (NSString*)appNameHashTag {
    return appNameHashTag;
}
+ (NSString*)appNameStr {
    return appNameStr;
}
+ (NSString*)getquestionwithoptionbylevelid {
    return getquestionwithoptionbylevelid;
}
+ (NSString*)adMobID {
    return adMobID;
}
+ (NSString*)getquestionrightanswerbylevelid {
    return getquestionrightanswerbylevelid;
}
+(NSString*)getLevel{
    return getLevel;
}
+(NSString*)getIsLocalPlay
{
   
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable)
    {
        return @"false";
        
    }
    else
    {
         return @"true";
        
    }

}


@end
