//
//  Function.m
// Trivia for ios

#import "Function.h"
#import "staticstrings.h"

@implementation Function

+ (NSMutableArray *)getAllData {
    NSMutableArray *allDataArray = [[NSMutableArray alloc] initWithCapacity:1];
    NSMutableArray *tempDiscussions = [Function getAllTask];
    [allDataArray addObjectsFromArray:tempDiscussions];
    return allDataArray;
}


/* ================= Event ================
 ===================       ================
 */

+ (NSMutableArray *)getQuestionsByLevel:(NSInteger*)firstIndex  questionId:(NSInteger*)lastIndex
{

    NSString *queryString = @"SELECT * FROM questions where question_id>=";
    queryString = [NSString stringWithFormat:@"%@%d%@%d", queryString, firstIndex,@" and question_id <= ",lastIndex];
    NSMutableArray *returndata=[DBOperation selectData:queryString];

    return returndata;
}

+(NSMutableArray *)getQuestionByCategory:(NSInteger)categoryId;
{
    NSString *sql=[NSString stringWithFormat:@"SELECT * FROM questions where quiz_level=%ld",(long)categoryId];
    NSMutableArray *returndata=[DBOperation selectData:sql];
    return returndata;
}


+(NSMutableArray *)getlevel:(NSString *)levelId;
{
    NSString *sql=[NSString stringWithFormat:@"SELECT * FROM quiz_level where Level_id =%@",levelId];
    NSMutableArray *returndata=[DBOperation selectData:sql];
    return returndata;
}


+(NSMutableArray *)getQuestion:(NSString *)question;
{
    NSString *sql=[NSString stringWithFormat:@"SELECT * FROM questions where question =%@",question];
    NSMutableArray *returndata=[DBOperation selectData:sql];
    return returndata;
}

+ (NSMutableArray *)getLevelInfo
{
    NSString *QueryString =@"SELECT * FROM quiz_level";
    QueryString = [NSString stringWithFormat:@"%@", QueryString];
    NSMutableArray *returndata=[DBOperation selectData:QueryString];
    return returndata;
}

+ (NSMutableArray *)getAnswer
{
    NSString *QueryString =@"SELECT option_a FROM questions";
    QueryString = [NSString stringWithFormat:@"%@", QueryString];
    NSMutableArray *returndata=[DBOperation selectData:QueryString];
    return returndata;
}

+ (NSMutableArray *)getAllTask
{
    NSString *strfilter = [NSString stringWithFormat:@"SELECT * FROM questions order by random() limit 10 "];
	NSMutableArray *returndata=[DBOperation selectData:strfilter];
	return returndata;
}
+ (int *)getCountTotalQuestion{
    NSString *strfilter = [NSString stringWithFormat:@"select count(*) from questions"];
    NSMutableArray *returndata=[DBOperation selectData:strfilter];
    NSString *name = [[returndata objectAtIndex:0] objectForKey:@"count(*)"];
    return [name intValue];
}

/* ================= Event ================
 =================         ================
 */




@end
