//
//  Function.h
// Trivia for ios

#import <UIKit/UIKit.h>

#import <Foundation/Foundation.h>
#import "DBOperation.h"

@interface Function : NSObject
{

}

+ (BOOL)updateTaskById:(NSInteger)taskId andTaskDictionary:(NSString *)isBookmark;

+ (NSMutableArray *)getAllData;
+ (NSMutableArray *)getQuestionsByLevel:(NSInteger*)firstIndex  questionId:(NSInteger*)lastIndex;
+ (NSMutableArray *)getQuestions;
+ (int *)getCountTotalQuestion;
+ (NSMutableArray *)getAllTask;
+ (NSMutableArray *)getAllOPtions;
+ (NSMutableArray *)getAnswer;
+ (NSMutableArray *)getdayevent;
+ (NSMutableArray *)getLevelInfo;
+(NSMutableArray *)getQuestion:(NSString *)question;
+ (NSMutableArray *)getQuestionByCategory:(NSInteger)categoryId;
+(NSMutableArray *)getlevel:(NSString *)levelId;


@end
