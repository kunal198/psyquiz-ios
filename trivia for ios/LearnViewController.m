//
//  ViewController.m
// Trivia for ios

#import "LearnViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <Social/Social.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "Function.h"
#import "staticstrings.h"
#import "AppDelegate.h"

#import <GoogleMobileAds/GADInterstitial.h>




@interface LearnViewController ()
    @property(nonatomic, strong) GADInterstitial *interstitial;
@end

@implementation LearnViewController
{
     NSMutableArray *qustns;
     NSMutableArray *ansr;
     AppDelegate *session;
     int count;
}

int total=0;
int qustnIndex =0;

@synthesize qustn,ans,levels,jump,answer;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    
    
    ans.layer.borderColor=[[UIColor grayColor] CGColor];
    ans.layer.borderWidth = 1;
    ans.layer.cornerRadius=5;
    answer.layer.cornerRadius = answer.frame.size.height / 2;
    answer.layer.masksToBounds = YES;
    
    session = [[UIApplication sharedApplication]delegate];
    [self createAndLoadInterstitial];
    count = (int)[Function getCountTotalQuestion];
    self.qustns = [[NSMutableArray alloc]init];
    qustnIndex=0;
    [self loadData];
    int total = [self.qustns count];
    
    NSString *question = [[self.qustns objectAtIndex:qustnIndex] objectForKey:@"question"];
    qustn.text = [[self.qustns objectAtIndex:qustnIndex] objectForKey:@"question"];
    self.levels.text = [NSString stringWithFormat:@"%d /%d",qustnIndex+1,total];
    
    //UI Design
    qustn.backgroundColor = [UIColor whiteColor];
//    [[self.qustn layer] setBorderColor:[[UIColor colorWithRed:255/255.0 green:179/255.0 blue:0/255.0 alpha:1] CGColor]];
//    [[self.qustn layer] setBorderWidth:2.0];
//    [[self.qustn layer] setCornerRadius:15];
    self.qustn.textAlignment = NSTextAlignmentCenter;
 
    ans.backgroundColor = [UIColor whiteColor];
//    [[self.ans layer] setBorderColor:[[UIColor colorWithRed:255/255.0 green:179/255.0 blue:0/255.0 alpha:1] CGColor]];
//    [[self.ans layer] setBorderWidth:2.0];
//    [[self.ans layer] setCornerRadius:15];
    
    UISwipeGestureRecognizer * Swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    Swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:Swipeleft];
    
    UISwipeGestureRecognizer * Swiperight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperight:)];
    Swiperight.direction=UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:Swiperight];

}
- (GADInterstitial *)createAndLoadInterstitial {
    self.interstitial = [[GADInterstitial alloc] init];
    self.interstitial.adUnitID = [staticstrings adMobID];
    self.interstitial.delegate = self;
    
    GADRequest *request = [GADRequest request];
    // Requests test ads on test devices.
    //request.testDevices = @[@"2077ef9a63d2b398840261c8221a0c9b"];
    [self.interstitial loadRequest:request];
    return self.interstitial;
}

//Get database
-(void)loadData{
    NSString *isPlayLocal = [staticstrings getIsLocalPlay];
    if ([isPlayLocal isEqualToString:@"true"]) {
        self.qustns = [Function getQuestionByCategory:1];              //Get Questions by Category from local database
    }else{
        self.qustns = [self GetQuestionList];                          //Get Questions by Category from server
    }
}

//Get Questions from server
-(NSMutableArray*)GetQuestionList{
    NSString *questionurl = [NSString stringWithFormat:@"%@%d",[staticstrings getquestionrightanswerbylevelid],self.selectedIndex ];
    NSMutableString *URL1 = [[NSMutableString alloc]initWithString:questionurl];
    NSURL *url1 = [NSURL URLWithString:URL1];
    NSError *error1 = nil;
    NSStringEncoding encoding1;
    NSString *jsonreturn1 = [[NSString alloc] initWithContentsOfURL:url1 usedEncoding:&encoding1 error:&error1];
    if (!jsonreturn1)
    {
        UIAlertView *alertView1 = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                             message:@"There is no server response please try again later."
                                                            delegate:self
                                                   cancelButtonTitle:nil
                                                   otherButtonTitles:@"OK" ,nil];
        [alertView1 show];
        [alertView1 release];
        return nil;
    }
    else
    {
        NSMutableArray *dict1 = [jsonreturn1 JSONValue];
        return dict1;
    }
}

//Next Button Event
- (IBAction)Backbutton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)nextQstn:(id)sender
{
    if([self.qustns count] >=1)
    {
        int total = [self.qustns count];
        qustnIndex++;
        if(qustnIndex<[self.qustns count] ){
        qustn.text = [[self.qustns objectAtIndex:qustnIndex] objectForKey:@"question"];
        self.levels.text = [NSString stringWithFormat:@"%d /%d",qustnIndex+1,total];
        NSString *name = [[self.qustns objectAtIndex:qustnIndex] objectForKey:@"question"];
    }
        else{
        qustnIndex=0;
        qustn.text = [[self.qustns objectAtIndex:qustnIndex] objectForKey:@"question"];
        self.levels.text = [NSString stringWithFormat:@"%d /%d",qustnIndex+1,total];
        NSString *name = [[self.qustns objectAtIndex:qustnIndex] objectForKey:@"question"];
    }
        if(qustnIndex%19==0){
            if ([self.interstitial isReady]) {
                [self.interstitial presentFromRootViewController:self];
            }
        }

    }
    ans.text =@"ANS: ";
}

- (void)interstitialDidDismissScreen:(GADInterstitial *)interstitial {
    self.interstitial = [self createAndLoadInterstitial];
}

//Previous Button Event
-(IBAction)preQstn:(id)sender
{
    if(qustnIndex >=1)
    {
        int total = [self.qustns count];
        qustnIndex--;
        if(qustnIndex<[self.qustns count] )
        qustn.text = [[self.qustns objectAtIndex:qustnIndex] objectForKey:@"question"];
        self.levels.text = [NSString stringWithFormat:@"%d /%d",qustnIndex+1,total];
        NSString *name = [[self.qustns objectAtIndex:qustnIndex] objectForKey:@"question"];
    }
    else if(qustnIndex<=0)
    {
        int total = [self.qustns count];
        qustn.text = [[self.qustns objectAtIndex:qustnIndex] objectForKey:@"question"];
        self.levels.text = [NSString stringWithFormat:@"%d /%d",qustnIndex+1,total];
        NSString *name = [[self.qustns objectAtIndex:qustnIndex] objectForKey:@"question"];
    }
    ans.text =@"ANS: ";
}

//change previous Question by swipe screen
-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
    ans.text =@"ANS: ";
    if([self.qustns count] >=1)
    {
        int total = [self.qustns count];
        qustnIndex++;
        if(qustnIndex<[self.qustns count] ){
            qustn.text = [[self.qustns objectAtIndex:qustnIndex] objectForKey:@"question"];
            self.levels.text = [NSString stringWithFormat:@"%d /%d",qustnIndex+1,total];
            NSString *name = [[self.qustns objectAtIndex:qustnIndex] objectForKey:@"question"];
        }else{
            qustnIndex=0;
            qustn.text = [[self.qustns objectAtIndex:qustnIndex] objectForKey:@"question"];
            self.levels.text = [NSString stringWithFormat:@"%d /%d",qustnIndex+1,total];
            NSString *name = [[self.qustns objectAtIndex:qustnIndex] objectForKey:@"question"];
        }
        if(qustnIndex%19==0){
            if ([self.interstitial isReady]) {
                [self.interstitial presentFromRootViewController:self];
            }
        }
        
    }
    
}
//change next Question by swipe screen
-(void)swiperight:(UISwipeGestureRecognizer*)gestureRecognizer
{
    ans.text =@"Answer:";
    if(qustnIndex>=1)
    {
        int total = [self.qustns count];
        qustnIndex--;
        if(qustnIndex<[self.qustns count] )
        qustn.text = [[self.qustns objectAtIndex:qustnIndex] objectForKey:@"question"];
        self.levels.text = [NSString stringWithFormat:@"%d /%d",qustnIndex+1,total];
        NSString *name = [[self.qustns objectAtIndex:qustnIndex] objectForKey:@"question"];
    }
    else if(qustnIndex<=0)
    {
        qustn.text = [[self.qustns objectAtIndex:qustnIndex] objectForKey:@"question"];        
        self.levels.text = [NSString stringWithFormat:@"%d /%d",qustnIndex+1,total];
        NSString *name = [[self.qustns objectAtIndex:qustnIndex] objectForKey:@"question"];
    }
}


  //ALERT VIEW
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
  {
    int total = [self.qustns count];
    NSString *enterNumber = [[alertView textFieldAtIndex:0] text];
    if([enterNumber intValue]<=0)
    {
        return;
    }

    if([enterNumber intValue]>[self.qustns count])
    {
        return;
    }
    qustnIndex = [enterNumber intValue];
    qustnIndex--;
    qustn.text = [[self.qustns objectAtIndex:qustnIndex] objectForKey:@"question"];
    self.levels.text = [NSString stringWithFormat:@"%d /%d",qustnIndex+1,total];
    ans.text =@"Answer: ";
}

//Jump Button Event
- (IBAction)jumpButtonClicked:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Question Change" message:@"Enter Question Number:" delegate:self cancelButtonTitle:@"OK"otherButtonTitles:nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [[alert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeNumberPad];
    [[alert textFieldAtIndex:0] becomeFirstResponder];
    [alert show];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//Show Answer button Event
- (IBAction)AnswerButtonClicked:(id)sender
{
    NSString *isPlayLocal = [staticstrings getIsLocalPlay];
    NSString *ansrs = nil;
    if ([isPlayLocal isEqualToString:@"true"]) {
        NSString *tempAns=[[self.qustns objectAtIndex:qustnIndex] objectForKey:@"RightAns"];
        if ([tempAns caseInsensitiveCompare:@"A"]){
            ansrs= [[self.qustns objectAtIndex:qustnIndex] objectForKey:@"OptionA"];
        }
        if ([tempAns caseInsensitiveCompare:@"B"]){
            ansrs= [[self.qustns objectAtIndex:qustnIndex] objectForKey:@"OptionB"];
        }
        if ([tempAns caseInsensitiveCompare:@"C"]){
            ansrs= [[self.qustns objectAtIndex:qustnIndex] objectForKey:@"OptionC"];
        }
        if ([tempAns caseInsensitiveCompare:@"D"]){
            ansrs= [[self.qustns objectAtIndex:qustnIndex] objectForKey:@"OptionD"];
        }
    }
     else{
         ansrs= [[self.qustns objectAtIndex:qustnIndex] objectForKey:@"rightans"];
    }
    NSString *ansstr = @"ANS: ";
    ansstr = [NSString stringWithFormat:@"%@%@", ansstr, ansrs];
    ans.text=ansstr;
}


- (void)dealloc {
    [_showAnswerOutlet release];
    [super dealloc];
}
@end
