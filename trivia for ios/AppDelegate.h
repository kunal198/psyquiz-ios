//
//  AppDelegate.h
//  Trivia for ios


//  Copyright (c) 2015 Suresh Kerai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong)NSString *selectedQuestionType;
@property (nonatomic, strong)NSString *CurrentLevel;
@property (nonatomic, strong)NSData *SynchDataScore;
@property (nonatomic, strong)NSString *loginstatus;
@property (nonatomic, strong)NSString *quizCompleteStatus;
@property (nonatomic, strong)NSString *QuizQueCount;
@property (nonatomic, strong)NSMutableArray *QuizCurrentArray;
@property (nonatomic, strong)NSString *quizrightQue;
@property (nonatomic, strong)NSString *quizwrongque;
@property (nonatomic, strong)NSString *totalScore;
@property (nonatomic, strong)NSString *CCompleteStatus;
@property (nonatomic, strong)NSString *CQueCount;
@property (nonatomic, strong)NSMutableArray *CCurrentArray;
@property (nonatomic, strong)NSString *CrightQue;
@property (nonatomic, strong)NSString *Cwrongque;
@property (nonatomic, strong)NSString *CtotalScore;
@end

