//
//  CurrentAffairScoreViewController.h
// Trivia for ios


#import <UIKit/UIKit.h>

@protocol SecondViewControllerDelegate <NSObject>

@required
- (void)dataFromController:(NSString *)data;

@end

@interface CurrentAffairScoreViewController : UIViewController

@property (nonatomic, retain) NSString *data;
@property (nonatomic, strong) id<SecondViewControllerDelegate> delegate;
@property(nonatomic,retain)IBOutlet UILabel *lblFinish;
@property(nonatomic,retain)IBOutlet UILabel *totalScore;
@property(nonatomic,retain)IBOutlet UILabel *levelScore;
@property(nonatomic,retain)IBOutlet UIButton *btnshare;
@property(nonatomic,retain)IBOutlet UIButton *btnPlayagain;
@property(nonatomic,strong) id infoRequest;
@property (retain, nonatomic) IBOutlet UIButton *homeButtonOutlet;
@property (retain, nonatomic) IBOutlet UIButton *tryAgainBtnOutlet;
@property (retain, nonatomic) IBOutlet UILabel *lvlCmpleteMsg;
@property (retain, nonatomic) IBOutlet UILabel *riteScore;
@property (retain, nonatomic) IBOutlet UILabel *wrongScore;
 @property(nonatomic,retain)  NSString *ritScore;
 @property(nonatomic,retain)  NSString *wrngScore;
- (IBAction)BackHome:(UIButton *)sender;
-(IBAction)Share:(id)sender;

@end
